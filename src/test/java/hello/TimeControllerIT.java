package hello;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TimeControllerIT {

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/get_time");
    }

    @Test
    public void getTime()  {
        long millis = System.currentTimeMillis();
        Date date = new Date(millis);

        ResponseEntity<String> response = template.getForEntity(base.toString(),
                String.class);

        String body = response.getBody();

        int index = body.lastIndexOf(":");
        String time = body.substring(0,index);
        String expectedValue = date.toString().substring(0,index);

        assertEquals("verify date", expectedValue,time);
    }
}
