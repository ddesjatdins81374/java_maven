package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@RestController
public class TimeController {

    @RequestMapping("/get_time")
    public String index() {
        long millis = System.currentTimeMillis();
        Date date = new Date(millis);
        return date.toString();
    }

}
